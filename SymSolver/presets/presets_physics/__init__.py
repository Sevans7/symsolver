"""
Package Purpose: preset SymbolicObjects directly related to physics

Uses classes defined elsewhere to pre-define some objects for convenience.

This file:
Imports the main important objects throughout this subpackage.
"""

from . import presets_em
from . import presets_em_vars
from . import presets_fluid_vars
from . import presets_fluids
from . import presets_physical_constants
from . import presets_plasma_quants
from . import presets_plasma_quants_vars
